import {
  mobile,
  backend,
  creator,
  web,
  javascript,
  typescript,
  html,
  css,
  reactjs,
  redux,
  tailwind,
  nodejs,
  mongodb,
  git,
  figma,
  docker,
  bankina,
  btp,
  agate,
  qlue,
  asyx,
  risdi,
  shopify,
  carrent,
  jobit,
  tripguide,
  google,
  redirect,
  threejs,
  linkedin,
  mail,
} from "../assets";

export const navLinks = [
  {
    id: "about",
    title: "About",
  },
  {
    id: "experience",
    title: "Experience",
  },
  {
    id: "certification",
    title: "Certification",
  },
  {
    id: "feedback",
    title: "Feedback",
  },
  // {
  //   id: "contact",
  //   title: "Contact",
  // },
];

const services = [
  {
    title: "DevOps Engineer",
    icon: jobit,
  },
  {
    title: "React Native Developer",
    icon: mobile,
  },
  {
    title: "Backend Developer",
    icon: backend,
  },
  {
    title: "Content Creator",
    icon: creator,
  },
];

const contactslogo = [
  {
    name: "mail",
    icon: mail,
  },
  {
    name: "linkedin",
    icon: linkedin,
  }
]

const technologies = [
  {
    name: "HTML 5",
    icon: html,
  },
  {
    name: "CSS 3",
    icon: css,
  },
  {
    name: "JavaScript",
    icon: javascript,
  },
  {
    name: "TypeScript",
    icon: typescript,
  },
  {
    name: "React JS",
    icon: reactjs,
  },
  {
    name: "Redux Toolkit",
    icon: redux,
  },
  {
    name: "Tailwind CSS",
    icon: tailwind,
  },
  {
    name: "Node JS",
    icon: nodejs,
  },
  {
    name: "MongoDB",
    icon: mongodb,
  },
  {
    name: "Three JS",
    icon: threejs,
  },
  {
    name: "git",
    icon: git,
  },
  {
    name: "figma",
    icon: figma,
  },
  {
    name: "docker",
    icon: docker,
  },
];

const experiences = [
  {
    title: "DevOps Engineer",
    company_name: "Bank INA Perdana",
    icon: bankina,
    iconBg: "#383E56",
    date: "Mei 2023 - Now",
    points: [
      "Deploying and maintaining app to production site using GitOps methodolgy.",
      "Collaborating with cross-functional teams including developers, and other teams to deliver high-quality products.",
      "Migration of VM based deployment application to Kubernetes.",
    ],
  },
  {
    title: "DevOps Engineer",
    company_name: "ASYX Indonesia",
    icon: asyx,
    iconBg: "#383E56",
    date: "Mei 2022 - Mei 2023",
    points: [
      "Experiences in managed and deploying microservices to Kubernetes (RKE, OCP, GKE).",
      "Enabled and Implement GitOps Methodolgy.",
      "Implement Interconnection between cluster (submariner) for cross communication in different cluster.",
      "Create Jenkins job pipelines to backup data statefulset applications and upgrading staging version application based on internal needs.",
    ],
  },
  {
    title: "DevOps Engineer",
    company_name: "Qlue Smart City",
    icon: qlue,
    iconBg: "#E6DEDD",
    date: "June 2021 - Mei 2022",
    points: [
      "Dockerize internal application for better and efficient deployment.",
      "Research and Development Kubernetes to migrate docker based deployment to kubernetes",
      "Designed and Implemented automation deployment using Jenkins and Ansible to 34 server clients to deliver the newest web application.",
      "Designed and Implemented monitoring tools (Grafana, Prometheus, Portainer, Alert Manager, InfluxDB and Telegraf) integrated with discord for alert management.",
    ],
  },
  {
    title: "Infrastructure and Networking Intern",
    company_name: "Qlue Smart City",
    icon: qlue,
    iconBg: "#383E56",
    date: "March 2021 - June 2023",
    points: [
      "Create topology for schema CCTV, NVR placement and cable management for client needs.",
      "Installed and Maintained RTSP Server for establishing and controlling media sessions between endpoints.",
      "Deployed and Dockerized zoneminder (a web application dashboard for collecting RTSP from CCTV in one place) in internal and external server.",
      "Setup and Maintained PRITUNL VPN Server for internal needs.",
    ],
  },
  {
    title: "DevOps Engineer Intern",
    company_name: "Agate International",
    icon: agate,
    iconBg: "#E6DEDD",
    date: "October 2020 - December 2020",
    points: [
      "Setup New Wordpress on EC2 Instance for Code Atma Project.",
      "Grabbed AWS CloudWatch Metrics for Cloud front, Elasticbeanstalk using AWS Python SDK (Boto3).",
      "Send collected metrics to Zabbix 5.0.",
    ],
  },
  {
    title: "DevOps Engineer Intern",
    company_name: "Bandung Techno Park",
    icon: btp,
    iconBg: "#E6DEDD",
    date: "June 2020 - August 2020",
    points: [
      "Set up EC2 Instance on AWS.",
      "Set up Jenkins and Gitlab runner on previous EC2 Instance.",
      "Set up Portainer for managing docker container.",
    ],
  },
];

const testimonials = [
  {
    testimonial:
      "Gilang Wibowo is a talented young man, having good communication so far there have been no problems when discussing with him, Gilang as a fast learner and likes the latest technological research to be implemented certainly greatly contributes to the company, I'm sure at a young age his career will be more successful. Hopefully Gilang can consistently continue to learn, I highly recommend Gilang being able to contribute to your team, I think that's a very extraordinary thing.",
    name: "Risdi Ramadhan On LinkedIn",
    designation: "Senior QA Engineer",
    company: "Blue Bird Group",
    image: risdi,
  },

];

const projects = [
  // {
  //   name: "Car Rent",
  //   description:
  //     "Web-based platform that allows users to search, book, and manage car rentals from various providers, providing a convenient and efficient solution for transportation needs.",
  //   tags: [
  //     {
  //       name: "react",
  //       color: "blue-text-gradient",
  //     },
  //     {
  //       name: "mongodb",
  //       color: "green-text-gradient",
  //     },
  //     {
  //       name: "tailwind",
  //       color: "pink-text-gradient",
  //     },
  //   ],
  //   image: carrent,
  //   source_code_link: "https://github.com/",
  // },
  // {
  //   name: "Job IT",
  //   description:
  //     "Web application that enables users to search for job openings, view estimated salary ranges for positions, and locate available jobs based on their current location.",
  //   tags: [
  //     {
  //       name: "react",
  //       color: "blue-text-gradient",
  //     },
  //     {
  //       name: "restapi",
  //       color: "green-text-gradient",
  //     },
  //     {
  //       name: "scss",
  //       color: "pink-text-gradient",
  //     },
  //   ],
  //   image: jobit,
  //   source_code_link: "https://github.com/",
  // },
  {
    name: "Professional Cloud Architect",
    description:
      "A Professional Cloud Architect is a certified expert responsible for designing and implementing Google Cloud Platform (GCP) solutions. They possess advanced knowledge of cloud architecture, infrastructure design, security, and compliance, enabling them to create scalable, reliable, and highly available cloud solutions tailored to meet specific business needs.",
    tags: [
      {
        name: "GoogleCloud",
        color: "blue-text-gradient",
      },
      {
        name: "DevOps",
        color: "green-text-gradient",
      },
      {
        name: "Certified",
        color: "pink-text-gradient",
      },
    ],
    image: google,
    source_code_link: "https://google.accredible.com/8c9f0eb5-8f05-4d9b-b400-c548d6d645a4",
  },
];

export { services, technologies, experiences, testimonials, projects };
