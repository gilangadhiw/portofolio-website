import logo from "./logo.svg";
import backend from "./backend.png";
import creator from "./creator.png";
import mobile from "./mobile.png";
import web from "./web.png";
import github from "./github.png";
import menu from "./menu.svg";
import close from "./close.svg";

import css from "./tech/css.png";
import docker from "./tech/docker.png";
import figma from "./tech/figma.png";
import git from "./tech/git.png";
import html from "./tech/html.png";
import javascript from "./tech/javascript.png";
import mongodb from "./tech/mongodb.png";
import nodejs from "./tech/nodejs.png";
import reactjs from "./tech/reactjs.png";
import redux from "./tech/redux.png";
import tailwind from "./tech/tailwind.png";
import typescript from "./tech/typescript.png";
import threejs from "./tech/threejs.svg";


import shopify from "./company/shopify.png";
import bankina from "./company/bankina.jpeg";
import btp from "./company/btp.jpeg";
import asyx from "./company/asyx.png";
import risdi from "./company/risdi.jpeg";
import qlue from "./company/qlue.png";
import agate from "./company/agate.jpeg";
import mail from "./company/mail.png";
import linkedin from "./company/linkedin.png";

import carrent from "./carrent.png";
import jobit from "./jobit.png";
import tripguide from "./tripguide.png";
import google from "./company/google-certified.png";
import redirect from "./company/link.jpg";

export {
  logo,
  backend,
  creator,
  mobile,
  web,
  github,
  menu,
  close,
  css,
  docker,
  figma,
  git,
  html,
  javascript,
  mongodb,
  nodejs,
  reactjs,
  redux,
  tailwind,
  typescript,
  threejs,
  shopify,
  bankina,
  btp,
  agate,
  qlue,
  asyx,
  risdi,
  carrent,
  jobit,
  tripguide,
  linkedin,
  mail,
  google,
  redirect,
};
