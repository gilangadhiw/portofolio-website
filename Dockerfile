# build environment
FROM node:18.16-alpine

# set working directory for base
WORKDIR /app

# set base PATH
ENV PATH /app/node_modules/.bin:$PATH
RUN apk add --no-cache git

COPY ./package.json /app/package.json

# begin build project
RUN npm install --force
COPY . .
RUN npm run build   

# install nginx
FROM nginx:stable

EXPOSE 80

WORKDIR /usr/share/nginx/html

RUN rm /etc/nginx/conf.d/default.conf

COPY nginx/nginx.conf /etc/nginx/nginx.conf
COPY --from=0 /app/dist /usr/share/nginx/html

CMD ["/bin/bash", "-c", "nginx"]
